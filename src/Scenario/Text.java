package Scenario;

public class Text {
    private String exposition;
    private String filmSet;
    private String scrap;
    private String conclusion;

    public Text(String exposition, String filmSet, String scrap, String conclusion) {
        this.exposition = exposition;
        this.filmSet = filmSet;
        this.scrap = scrap;
        this.conclusion = conclusion;
    }

    public String getExposition() {
        return exposition;
    }

    public String getFilmSet() {
        return filmSet;
    }

    public String getScrap() {
        return scrap;
    }

    public String getConclusion() {
        return conclusion;
    }
}
