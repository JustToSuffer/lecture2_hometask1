package Scenario;

import People.Person;

public class Scenario {
    private String title;
    private Text text;
    private String date;
    private Person author;

    public Scenario(String title, Text text, String date, Person author) {
        this.title = title;
        this.text = text;
        this.date = date;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public Text getText() {
        return text;
    }

    public String getDate() {
        return date;
    }

    public Person getAuthor() {
        return author;
    }
}
