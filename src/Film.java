import People.Character;
import People.Person;
import Scenario.Scenario;

public class Film {
    private String title;
    private Character[] character;
    private String editing;
    private Scenario scenario;
    private Person director;


    public Film(String title, Character[] character, String editing, Scenario scenario, Person director) {
        this.title = title;
        this.character = character;
        this.editing = editing;
        this.scenario = scenario;
        this.director = director;
    }

    public String getTitle() {
        return title;
    }

    public Character[] getCharacter() {
        return character;
    }

    public String getEditing() {
        return editing;
    }

    public Scenario getScenario() {
        return scenario;
    }

    public Person getDirector() {
        return director;
    }
}
