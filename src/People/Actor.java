package People;

public class Actor {
    private Person person;
    private int numberOfAwards;
    private int numberOfMovies;

    public Actor(Person person, int numberOfAwards, int numberOfMovies) {
        this.person = person;
        this.numberOfAwards = numberOfAwards;
        this.numberOfMovies = numberOfMovies;
    }

    public Person getPerson() {
        return person;
    }

    public int getNumberOfAwards() {
        return numberOfAwards;
    }

    public int getNumberOfMovies() {
        return numberOfMovies;
    }
}
