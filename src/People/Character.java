package People;

public class Character {
    private String name;
    private Actor actor;

    public Character(String name, Actor actor) {
        this.name = name;
        this.actor = actor;
    }

    public String getName() {
        return name;
    }

    public Actor getActor() {
        return actor;
    }
}