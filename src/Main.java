import People.Actor;
import People.Character;
import People.Person;
import Scenario.Scenario;
import Scenario.Text;

public class Main {
    public static void main(String[] args) {
        Person elijahWood = new Person("Elijah", "Wood", "28.01.1981");
        Actor actorElijahWood = new Actor(elijahWood, 12, 30);
        Person peterJackson = new Person("Peter", "Jackson", "31.10.1961");
        Person ianMcKellen = new Person("Ian", "McKellen", "25.05.1939");
        Actor actorIanMcKellen = new Actor(ianMcKellen, 50, 100);
        Scenario lord = new Scenario("lotr", new Text("textExposition", "textFilmSet",
                "textScrap", "textConclusion"), "19.01.2001", peterJackson);
        Film lordOfTheRings = new Film("LordOfTheRings", new Character[]{new Character("Frodo", actorElijahWood),
                new Character("Gendalf", actorIanMcKellen)}, "Great", lord, peterJackson);

        Person markHamill = new Person("Mark", "Hamill", "25.09.1951");
        Actor actorMarkHamill = new Actor(markHamill, 17, 15);
        Person georgeLucas = new Person("George", "Lucas", "14.05.1944");
        Person harrisonFord = new Person("Harrison", "Ford", "13.07.1942");
        Actor actorHarrisonFord = new Actor(harrisonFord, 40, 95);
        Scenario wars = new Scenario("sw", new Text("textExposition", "textFilmSet",
                "textScrap", "textConclusion"), "19.01.1977", georgeLucas);
        Film starWars = new Film("StarWars", new Character[]{new Character("Luke", actorMarkHamill),
                new Character("Solo", actorHarrisonFord)}, "Good", wars, georgeLucas);

        System.out.println(starWars.getDirector().getFullName());
        System.out.println(lordOfTheRings.getCharacter()[1].getName() + " = " + lordOfTheRings.getCharacter()[1].getActor().getPerson().getFullName());
    }
}
